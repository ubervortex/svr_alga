clear
load('TrainingSet_6.mat')
% SVR Regression for estimating the concentration of Catenella

F1 = normalize(train_final(:,4),'range');
F2 = normalize(train_final(:,6),'range');
F3 = normalize(train_final(:,7),'range');
F4 = normalize(train_final(:,8),'range');
F5 = normalize(train_final(:,1),'range');
F6 = normalize(train_final(:,2),'range');
F7 = normalize(train_final(:,9),'range');


pos_class1 = find(train_final(:,5)>0);
T = train_final(:,5);
T(pos_class1) = 1;
tbl = table(F1,F2,F3,F4,F5,F6,F7,T);
N = size(tbl,1);

rng(10); % For reproducibility
cvp = cvpartition(N,'Holdout',0.20);
idxTrn = training(cvp); % Training set indices
idxTest = test(cvp);    % Test set indices

% training SVR regression
Mdl = fitcsvm(tbl(idxTrn,:),'T','KernelFunction','gaussian','KernelScale','auto','Standardize',true,'OutlierFraction',0.05);

% estimation SVR regression
YFit = predict(Mdl,tbl(idxTest,:));

table(tbl.T(idxTest),YFit,'VariableNames',{'ObservedValue','PredictedValue'})

conv = Mdl.ConvergenceInfo.Converged;
iter = Mdl.NumIterations;

X = tbl(idxTrn,:);

MdlGau = fitrsvm(X,'T','Standardize',true,'KFold',19,'KernelFunction','gaussian')

mseGau = kfoldLoss(MdlGau)

diff = tbl.T(idxTest)-YFit;
ERR_PERC = 100*nnz(diff)/size(diff,1)



