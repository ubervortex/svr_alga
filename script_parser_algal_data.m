%% ----------------------------------------------------------------------%%
% Author: Colombo team
% Version: 1.0
% 
% Description: parser per dataset toxic algea Chile
% 
%  
% Input: Federica campaign dataset
% Output: training set 
%--------------------------------------------------------------------------

clear
close all
%[num,text,both]=xlsread('Reportes Marea Roja 2017_IFOP_species.xlsx');

% import concentration of toxic algal, dates, position 
load('Catenella_Fede.mat')
% convert format of dates
DateNum = x2mdate(str2double(Catenella_Fede(:,2)),0);
formatOut = 'yyyymmdd';
for i=1:size(DateNum,1)
    cat_dates(i,:) = datestr(DateNum(i),formatOut);
end
dates_cat = zeros(size(cat_dates,1),1);
for i=1:size(cat_dates,1)
    dates_cat(i) = str2num(datestr(DateNum(i),formatOut));
end
%dates_cat = Catenella_Fede(:,2);
cat_lat = Catenella_Fede(:,4);
cat_long = Catenella_Fede(:,5);
cat_conc = Catenella_Fede(:,1);

% The position of my data is included in this square shape [-75.9583,-56.1250;-69.8750,-50.0417]

for i=1:size(cat_conc,1)
    plot_conc(i) = str2double(cat_conc(i));
end
plot(plot_conc)
hold on

for i=1:size(cat_lat,1)
    C(1:3) = strsplit(cat_lat(i,1),' ');
    S1 = C(1);
    S1 = str2mat(S1);
    S1 = S1(1:end-1);
    S1 = str2num(S1);
    S2 = C(2);
    S2 = str2mat(S2);
    S2 = S2(1:end-1);
    S2 = str2num(S2);
    S3 = C(3);
    S3 = str2mat(S3);
    S3 = strrep(S3,',','.');
    S3 = S3(1:end-1);
    S3 = str2num(S3);
    angleInDegrees_lat(i) = dms2degrees([S1 S2 S3]);
end

for i=1:size(cat_long,1)
    C = strsplit(cat_long(i,1),' ');
    if size(C,2) == 4
        C = C(2:4);
    end
    S1 = C(1);
    S1 = str2mat(S1);
    S1 = S1(1:end-1);
    S1 = str2num(S1);
    S2 = C(2);
    S2 = str2mat(S2);
    S2 = S2(1:end-1);
    S2 = str2num(S2);
    S3 = C(3);
    S3 = str2mat(S3);
    S3 = strrep(S3,',','.');
    S3 = S3(1:end-1);
    S3 = str2num(S3);
    angleInDegrees_long(i) = dms2degrees([S1 S2 S3]);
end

% for i=1:size(angleInDegrees_lat)
%     if angleInDegrees_long(i)<55
%         angleInDegrees_long(i) = 0;
%     end    
% end


cat_conc = str2double(cat_conc);
for i=1:size(cat_conc)
    cat_dates_p(i) = str2double(cat_dates(i,1:8));
end

Training_data = [angleInDegrees_lat', angleInDegrees_long', cat_dates_p', DateNum, cat_conc];

[p,value] = find(Training_data(:,1)>-56 & Training_data(:,1)<-50 & Training_data(:,2)<-69 & Training_data(:,2)>-75);

temp_array = zeros(size(cat_conc));
temp_array(p) = 1; 
Training_data_fitered = temp_array .* Training_data;
%nnz(Training_data_fitered)
Training_data_fitered_F = Training_data_fitered(any(Training_data_fitered,2),:);
plot(cat_conc(p))

% estrarre valori dai dati

for i=1:size(Training_data_fitered_F,1)
    date_file = Training_data_fitered_F(i,3);
    %SST
    file_name_SST = ['/Users/davide/Documents/ColomboSky/SVM_python/10/CMEMS_int_SST/CMEMS_SST_',num2str(date_file),'.tif'];
    SST = geotiffread(file_name_SST);

    %Sal
    file_name_Sal = ['/Users/davide/Documents/ColomboSky/SVM_python/10/CMEMS_int_S/CMEMS_S_',num2str(date_file),'.tif'];
    Sal = geotiffread(file_name_Sal);

    %CHL
    file_name_CHL = ['/Users/davide/Documents/ColomboSky/SVM_python/10/CMEMS_int_CHL/CMEMS_CHL_',num2str(date_file),'.tif'];
    CHL = geotiffread(file_name_CHL); % da ricampionare
    
    % closest coordinates
    input_georef_measure = [Training_data_fitered_F(i,1) Training_data_fitered_F(i,2)];

    info = geotiffinfo(file_name_SST);
    [x,y]=pixcenters(info);
    XY_camp(i,:) = input_georef_measure;
    [pos_x, pos_y] = campains_locator(input_georef_measure, x, y); %to be checked!!
    SST_samples(i) = SST(pos_x,pos_y);
    Sal_samples(i) = Sal(pos_x,pos_y);
    info_CHL = geotiffinfo(file_name_CHL);
    [x_chl,y_chl]=pixcenters(info_CHL);
    [pos_x_chl, pos_y_chl] = campains_locator(input_georef_measure, x_chl, y_chl); %to be checked!!
    CHL_samples(i) = CHL(pos_x_chl,pos_y_chl);
    Training_data_fitered_F(i,6) = CHL(pos_x_chl,pos_y_chl);
    Training_data_fitered_F(i,7) = SST(pos_x,pos_y);
    Training_data_fitered_F(i,8) = Sal(pos_x,pos_y);
%     pos_CHL(i,:) = [pos_x_chl, pos_y_chl];
%     pos_Sal(i,:) = [pos_x, pos_y];
%     pos_SST(i,:) = [pos_x, pos_y];

end

%%
[r, c] = find(Training_data_fitered_F < -1000);
Training_data_fitered_F(r,:)=[];

% delete_samples_Sal = find(Sal_samples < -1000);
% Sal_samples(delete_samples_Sal)=[];
% delete_samples_SST = find(SST_samples < -1000);
% SST_samples(delete_samples_SST)=[];

% figure; plot((CHL_samples))
% figure; plot((Sal_samples))
% figure; plot((SST_samples))



 
% % apri immagine con data corrispondente
% SST = geotiffread('/Users/davide/Documents/ColomboSky/SVM_python/10/CMEMS_int_SST/CMEMS_SST_20180811.tif');
% info = geotiffinfo('/Users/davide/Documents/ColomboSky/SVM_python/10/CMEMS_int_SST/CMEMS_SST_20180811.tif');
% [x,y]=pixcenters(info);
% for i=1:size(Training_data_fitered_F,1)
%     input_georef_measure = [Training_data_fitered_F(i,1) Training_data_fitered_F(i,2)];
%     [pos_x, pos_y] = campains_locator(input_georef_measure, x, y); %to be checked!!
%     stat_coordinates(i,1)=pos_x;
%     stat_coordinates(i,2)=pos_y;
%     SST_samples(i) = SST(stat_coordinates(i,1),stat_coordinates(i,2));
% end
% % salva il valore correspondente alle coordinate lat long
% % salva valore di SST S and CHL
% delete_samples = find(SST_samples < -1000);
% SST_samples(delete_samples)=[];
% figure; plot(sort(SST_samples))
% 
% Sal = geotiffread('/Users/davide/Documents/ColomboSky/SVM_python/10/CMEMS_int_S/CMEMS_S_20180811.tif');
% info = geotiffinfo('/Users/davide/Documents/ColomboSky/SVM_python/10/CMEMS_int_S/CMEMS_S_20180811.tif');
% [x,y]=pixcenters(info);
% for i=1:size(Training_data_fitered_F,1)
%     input_georef_measure = [Training_data_fitered_F(i,1) Training_data_fitered_F(i,2)];
%     [pos_x, pos_y] = campains_locator(input_georef_measure, x, y); %to be checked!!
%     stat_coordinates(i,1)=pos_x;
%     stat_coordinates(i,2)=pos_y;
%     Sal_samples(i) = Sal(stat_coordinates(i,1),stat_coordinates(i,2));
% end
% % salva il valore correspondente alle coordinate lat long
% % salva valore di SST S and CHL
% delete_samples_Sal = find(Sal_samples < -1000);
% Sal_samples(delete_samples_Sal)=[];
% figure; plot(sort(Sal_samples))
% 
% Chl = geotiffread('/Users/davide/Documents/ColomboSky/SVM_python/10/CMEMS_int_CHL/CMEMS_CHL_20180811.tif');
% info = geotiffinfo('/Users/davide/Documents/ColomboSky/SVM_python/10/CMEMS_int_CHL/CMEMS_CHL_20180811.tif');
% [x,y]=pixcenters(info);
% for i=1:size(Training_data_fitered_F,1)
%     input_georef_measure = [Training_data_fitered_F(i,1) Training_data_fitered_F(i,2)];
%     [pos_x, pos_y] = campains_locator(input_georef_measure, x, y); %to be checked!!
%     stat_coordinates(i,1)=pos_x;
%     stat_coordinates(i,2)=pos_y;
%     Chl_samples(i) = Chl(stat_coordinates(i,1),stat_coordinates(i,2));
% end
% % salva il valore correspondente alle coordinate lat long
% % salva valore di SST S and CHL
% delete_samples_Sal = find(Chl_samples < -1000);
% Chl_samples(delete_samples_Sal)=[];
% figure; plot(sort(Chl_samples))
