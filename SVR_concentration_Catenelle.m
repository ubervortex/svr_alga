clear all
load('TrainingSet_6.mat')
% SVR Regression for estimating the concentration of Catenella

F1 = normalize(train_final(:,4),'range');
F2 = normalize(train_final(:,6),'range');
F3 = normalize(train_final(:,7),'range');
F4 = normalize(train_final(:,8),'range');
F5 = normalize(train_final(:,1),'range');
F6 = normalize(train_final(:,2),'range');

T = train_final(:,5);

tbl = table(F1,F2,F3,F4,F5,F6,T);

N = size(tbl,1);

rng(10); % For reproducibility
cvp = cvpartition(N,'Holdout',0.03);
idxTrn = training(cvp); % Training set indices
idxTest = test(cvp);    % Test set indices

% training SVR regression
Mdl = fitrsvm(tbl(idxTrn,:),'T','KernelFunction','rbf','KernelScale','auto','Standardize',true);

CVSVMModel = crossval(Mdl);

kfoldLoss(CVSVMModel)

FirstModel = CVSVMModel.Trained{1};

% estimation SVR regression
YFit = (predict(FirstModel,tbl(idxTest,:)));

table(tbl.T(idxTest),YFit,'VariableNames',{'ObservedValue','PredictedValue'})

conv = Mdl.ConvergenceInfo.Converged
iter = Mdl.NumIterations

% MdlGau = fitrsvm(tbl(idxTrn,:),'T','Standardize',true,'KFold',5,'KernelFunction','gaussian')
% mseGau = kfoldLoss(MdlGau)

figure; scatter(abs(YFit),tbl.T(idxTest),'marker','*')
xlabel('Estimate')
ylabel('Real')
hold on;

grid on
