%--------------------------------------------------------------------------
% description:  This functions return the position (in coordinate x,y) of 
%               of the points where ground measurements were acquired during 
%               during the in-situ campaigns. This information will be used 
%               in order to realize the feature matrix
%                       
% version 1.0
% date 2013/06/06
% author Davide Castelletti
%
%--------------------------------------------------------------------------
function [pos_x, pos_y] = campains_locator(input_georef_measure, tab_x, tab_y)

    E_diff = abs(tab_x - (input_georef_measure(1,2)));
    N_diff = abs(tab_y - (input_georef_measure(1,1)));
    [E_min, pos_x] = min(E_diff);
    [N_min, pos_y] = min(N_diff);