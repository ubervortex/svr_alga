clear
load('TrainingSet_6.mat')
% SVR Regression for estimating the concentration of Catenella

F1 = normalize(train_final(:,4),'range');
F2 = normalize(train_final(:,6),'range');
F3 = normalize(train_final(:,7),'range');
F4 = normalize(train_final(:,8),'range');
F5 = normalize(train_final(:,1),'range');
F6 = normalize(train_final(:,2),'range');
F7 = normalize(train_final(:,9),'range');

pos_class1 = find(train_final(:,5)>0);
T = train_final(:,5);
T(pos_class1) = 1;
tbl = table(F1,F2,F3,F4,F5,F6,F7,T);
N = size(tbl,1);

rng(10); % For reproducibility
cvp = cvpartition(N,'Holdout',0.2);
idxTrn = training(cvp); % Training set indices
idxTest = test(cvp);    % Test set indices


CVSVMModel = fitcsvm(tbl(idxTrn,:),'T','Holdout',0.15,'ClassNames',{'1','0'},'KernelFunction','gaussian','Standardize',true,'OutlierFraction',0.05);
CompactSVMModel = CVSVMModel.Trained{1}; % Extract the trained, compact classifier
testInds = test(CVSVMModel.Partition);   % Extract the test indices

XTest = tbl(idxTrn,:);
YTest = testInds;
L = loss(CompactSVMModel,XTest,YTest)

L = loss(CompactSVMModel,XTest,YTest,'LossFun','classiferror')